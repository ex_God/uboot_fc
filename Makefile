#
# Copyright (C) 2012 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk
include $(INCLUDE_DIR)/kernel.mk
include $(INCLUDE_DIR)/package.mk

PKG_NAME:=uboot_fc
PKG_VERSION:=0.1
PKG_RELEASE:=1

PKG_BUILD_DIR:=$(BUILD_DIR)/$(PKG_NAME)-$(PKG_VERSION)

define Package/uboot_fc
  MAINTAINER:=user in foxcomm
  TITLE:=u-boot for foxcomm
  URL:=http://.ru/
endef

define Package/uboot_fc
  TITLE:=u-boot for jaguar board
  CATEGORY:=Base system
endef

define Package/uboot_fc/description
description u-boot for jaguar board
endef

define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) ./src/* $(PKG_BUILD_DIR)/	-r
endef

define Build/Compile
	echo "#############"
	echo
	echo "build u-boot"
	echo
	echo
	echo 
	echo
	echo "##############"

	$(MAKE) -C $(PKG_BUILD_DIR) BASE=$(PKG_BUILD_DIR)
endef

define Package/uboot_fc/install
  cp $(PKG_BUILD_DIR)/u-boot.bin ../
endef

$(eval $(call BuildPackage,uboot_fc))
