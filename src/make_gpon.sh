#!/bin/bash
PATH=/home/dmitry/uboot/cross/bin/:$PATH

# PART 1
echo " Making uboot for DB-88F6282A-BP LE [DDR3] "
make mrproper
#   -) NAND
# This is equivalent to DDR2 400 MHz
#make db88f6282bp_config NBOOT=1 [DDR3=1] [FREQ=533]

# This is equivalent to DDR3 533 MHz
#make db88f6282bp_config NBOOT=1 DDR3=1 FREQ=533
make db88f6282bp_config NBOOT=1 

#   -) SPI
#make db88f6282bp_config SPIBOOT=1 [DDR3=1] [FREQ=533 or 400]
#   -) SPI with NAND support
#make db88f6282bp_config SPIBOOT=1 [DDR3=1 NAND=1] [FREQ=533 or 400]
make -s

echo
echo " Image creation sequence done... Termination in 2 seconds."
sleep 2
################################################################

# PART 2

#echo " Image for RD-88F6282A-BP LE DDR3 "
#make mrproper
#   -) NAND
#make rd88f6282a_config NBOOT=1 DDR3=1
#make -s
#echo
#echo " Image creation sequence done...Termination in 2 seconds. "
#sleep 2


