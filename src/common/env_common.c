/*
 * (C) Copyright 2000-2002
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * (C) Copyright 2001 Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Andreas Heppel <aheppel@sysgo.de>

 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <command.h>
#include <environment.h>
#include <linux/stddef.h>
#include <malloc.h>

#include <nand.h>
#include <asm/byteorder.h>

#ifdef CONFIG_SHOW_BOOT_PROGRESS
# include <status_led.h>
# define SHOW_BOOT_PROGRESS(arg)	show_boot_progress(arg)
#else
# define SHOW_BOOT_PROGRESS(arg)
#endif

#ifdef CONFIG_AMIGAONEG3SE
	extern void enable_nvram(void);
	extern void disable_nvram(void);
#endif

#undef DEBUG_ENV
#ifdef DEBUG_ENV
#define DEBUGF(fmt,args...) printf(fmt ,##args)
#else
#define DEBUGF(fmt,args...)
#endif

extern env_t *env_ptr;

extern void env_relocate_spec (void);
extern uchar env_get_char_spec(int);

static uchar env_get_char_init (int index);
uchar (*env_get_char)(int) = env_get_char_init;

extern unsigned long int CFG_ENV_OFFSET;
extern unsigned long int CFG_ENV_ADDR;
extern unsigned char kern_1_ver[32];
extern unsigned char kern_2_ver[32];
extern nand_info_t nand_info[];       /* info for NAND chips */

#define	 FW_PATTERN	"Fw"

/************************************************************************
 * Default settings to be used when no valid environment is found
 */
#define XMK_STR(x)	#x
#define MK_STR(x)	XMK_STR(x)

uchar default_environment_env1[] = {
#if defined(CONFIG_BOOTDELAY) && (CONFIG_BOOTDELAY >= 0)
	"bootdelay="	MK_STR(CONFIG_BOOTDELAY)	"\0"
#endif
#ifdef	CONFIG_ETHADDR
	"ethaddr="	MK_STR(CONFIG_ETHADDR)		"\0"
#endif
#ifdef	CONFIG_ETH1ADDR
	"eth1addr="	MK_STR(CONFIG_ETH1ADDR)		"\0"
#endif
#ifdef	CONFIG_ETH2ADDR
	"eth2addr="	MK_STR(CONFIG_ETH2ADDR)		"\0"
#endif
#ifdef	CONFIG_IPADDR
	"ipaddr="	MK_STR(CONFIG_IPADDR)		"\0"
#endif
#ifdef	CONFIG_SERVERIP
	"serverip="	MK_STR(CONFIG_SERVERIP)		"\0"
#endif
"dev_name=jaguar\0"\
"hw_ver=Hw1.0.1\0"\
"kernel_ver=no_define\0"\
"new_fw_cnt=no_define\0"\
"boot_priority=5\0"\
"mtdids=nand0=nand\0"\
"mtdparts=mtdparts=nand:0x80000@0x00000(u-boot),0x40000@0x80000(env1),0x40000@0xc0000(env2),"\
	"0x500000@0x100000(kernel1),0x500000@0x600000(kernel2),0x2000000@0xb00000(ubifs1),0x2000000@0x2b00000(ubifs2)\0"\
"kerneladdr=0x10080000\0"\
"ubootfile=foxComm/mrv/u-boot.bin\0"\
"kernelfile=foxComm/mrv/uImage\0"\
"rootfsfile=foxComm/mrv/ubi.img\0"\
"_tftpget-uboot=tftp $(loadaddr) $(ubootfile)\0"\
"_tftpget-kernel=tftp $(loadaddr) $(kernelfile)\0"\
"_tftpget-rootfs=tftp $(loadaddr) $(rootfsfile)\0"\
"_erase-uboot=nand erase clean u-boot; nand erase clean env1; nand erase clean env2;\0"\
"_erase-kernel=nand erase clean kernel1\0"\
"_erase-rootfs=nand erase clean ubifs1\0"\
"_nandwrite-uboot=nand write.e $(loadaddr) u-boot $(filesize)\0"\
"_nandwrite-kernel=nand write.e $(loadaddr) kernel1 $(filesize)\0"\
"_nandwrite-rootfs=nand write.e $(loadaddr) ubifs1 $(filesize)\0"\
"update-uboot=run _tftpget-uboot; run _erase-uboot; run _nandwrite-uboot \0"\
"update-kernel=run _tftpget-kernel; run _erase-kernel; run _nandwrite-kernel \0"\
"update-rootfs=run _tftpget-rootfs; run _erase-rootfs; run _nandwrite-rootfs \0"\
"bootcmd=nand read.e $(loadaddr) kernel1; bootm $(loadaddr) \0"\
"bootargs=console=ttyS0,115200 ubi.mtd=5 root=ubi0 rootfstype=ubifs \0"\
"\0"
};
//"bootcmd=tf 0x2000000 foxComm/mrv/uImage;tf 0x3000000 foxComm/mrv/ramdisk.img;"\
//		"bootm 0x2000000 0x3000000 \0"\
//"bootargs=console=ttyS0,115200 root=/dev/ram rw loglevel=8 \0"\
//"bootcmd=nand read.e $(loadaddr) kernel1;tf 0x3000000 foxComm/mrv/ramdisk.img; bootm $(loadaddr) 0x3000000\0"\
//"bootcmd=nand read.e $(loadaddr) kernel1; bootm $(loadaddr) \0"\
//"bootargs=console=ttyS0,115200 ubi.mtd=5 root=ubi0:rootfsU rootfstype=ubifs \0"\


uchar default_environment_env2[] = {
#if defined(CONFIG_BOOTDELAY) && (CONFIG_BOOTDELAY >= 0)
	"bootdelay="	MK_STR(CONFIG_BOOTDELAY)	"\0"
#endif
#ifdef	CONFIG_ETHADDR
	"ethaddr="	MK_STR(CONFIG_ETHADDR)		"\0"
#endif
#ifdef	CONFIG_ETH1ADDR
	"eth1addr="	MK_STR(CONFIG_ETH1ADDR)		"\0"
#endif
#ifdef	CONFIG_ETH2ADDR
	"eth2addr="	MK_STR(CONFIG_ETH2ADDR)		"\0"
#endif
#ifdef	CONFIG_IPADDR
	"ipaddr="	MK_STR(CONFIG_IPADDR)		"\0"
#endif
#ifdef	CONFIG_SERVERIP
	"serverip="	MK_STR(CONFIG_SERVERIP)		"\0"
#endif
"dev_name=jaguar\0"\
"hw_ver=Hw1.0.1\0"\
"kernel_ver=no_define\0"\
"new_fw_cnt=no_define\0"\
"boot_priority=4\0"\
"mtdids=nand0=nand\0"\
"mtdparts=mtdparts=nand:0x80000@0x00000(u-boot),0x40000@0x80000(env1),0x40000@0xc0000(env2),"\
	"0x500000@0x100000(kernel1),0x500000@0x600000(kernel2),0x2000000@0xb00000(ubifs1),0x2000000@0x2b00000(ubifs2)\0"\
"kerneladdr=0x10080000\0"\
"ubootfile=foxComm/mrv/u-boot.bin\0"\
"kernelfile=foxComm/mrv/uImage\0"\
"rootfsfile=foxComm/mrv/ubi.img\0"\
"_tftpget-uboot=tftp $(loadaddr) $(ubootfile)\0"\
"_tftpget-kernel=tftp $(loadaddr) $(kernelfile)\0"\
"_tftpget-rootfs=tftp $(loadaddr) $(rootfsfile)\0"\
"_erase-uboot=nand erase clean u-boot; nand erase clean env1; nand erase clean env2;\0"\
"_erase-kernel=nand erase clean kernel2\0"\
"_erase-rootfs=nand erase clean ubifs2\0"\
"_nandwrite-uboot=nand write.e $(loadaddr) u-boot $(filesize)\0"\
"_nandwrite-kernel=nand write.e $(loadaddr) kernel2 $(filesize)\0"\
"_nandwrite-rootfs=nand write.e $(loadaddr) ubifs2 $(filesize)\0"\
"update-uboot=run _tftpget-uboot; run _erase-uboot; run _nandwrite-uboot \0"\
"update-kernel=run _tftpget-kernel; run _erase-kernel; run _nandwrite-kernel \0"\
"update-rootfs=run _tftpget-rootfs; run _erase-rootfs; run _nandwrite-rootfs \0"\
"bootcmd=nand read.e $(loadaddr) kernel2; bootm $(loadaddr) \0"\
"bootargs=console=ttyS0,115200 ubi.mtd=6 root=ubi0 rootfstype=ubifs \0"\
"\0"
};

#if defined(CFG_ENV_IS_IN_NAND)		/* Environment is in Nand Flash */
int default_environment_size_env1 = sizeof(default_environment_env1);
int default_environment_size_env2 = sizeof(default_environment_env2);
#endif

void env_crc_update (void)
{
	env_ptr->crc = crc32(0, env_ptr->data, ENV_SIZE);
}

static uchar env_get_char_init (int index)
{
	DECLARE_GLOBAL_DATA_PTR;
	uchar c;

	/* if crc was bad, use the default environment */
	if (gd->env_valid)
	{
		c = env_get_char_spec(index);
	} else {
		if (CFG_ENV_OFFSET == UBOOT_ENV1){
			c = default_environment_env1[index];
		}
		else {
			c = default_environment_env2[index];
		}
	}

	return (c);
}

#ifdef CONFIG_AMIGAONEG3SE
uchar env_get_char_memory (int index)
{
	DECLARE_GLOBAL_DATA_PTR;
	uchar retval;
	enable_nvram();
	if (gd->env_valid) {
		retval = ( *((uchar *)(gd->env_addr + index)) );
	} else {
		retval = ( default_environment[index] );
	}
	disable_nvram();
	return retval;
}
#else
uchar env_get_char_memory (int index)
{
	DECLARE_GLOBAL_DATA_PTR;

	if (gd->env_valid) {
		return ( *((uchar *)(gd->env_addr + index)) );
	} else {
		if (CFG_ENV_OFFSET == UBOOT_ENV1){
			return ( default_environment_env1[index] );
		}
		else {
			return ( default_environment_env2[index] );
		}
	}
}
#endif

uchar *env_get_addr (int index)
{
	DECLARE_GLOBAL_DATA_PTR;

	if (gd->env_valid) {
		return ( ((uchar *)(gd->env_addr + index)) );
	} else {
		if (CFG_ENV_OFFSET == UBOOT_ENV1){
			return (&default_environment_env1[index]);
		}
		else {
			return (&default_environment_env2[index]);
		}
	}
}

int atoi(char *string)
{
    int res = 0;
    while (*string>='0' && *string <='9')
    {
	res *= 10;
	res += *string-'0';
	string++;
    }

    return res;
}

/* reverse:  переворачиваем строку s на месте */
void reverse(char s[])
{
    int i, j;
    char c;

    for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

/* itoa:  конвертируем n в символы в s */
 void itoa(int n, char s[])
 {
     int i, sign;

     if ((sign = n) < 0)  /* записываем знак */
         n = -n;          /* делаем n положительным числом */
     i = 0;
     do {       /* генерируем цифры в обратном порядке */
         s[i++] = n % 10 + '0';   /* берем следующую цифру */
     } while ((n /= 10) > 0);     /* удаляем */
     if (sign < 0)
         s[i++] = '-';
     s[i] = '\0';
     reverse(s);
 }

 void switch_to_another_partition (char part_num)
 {
	 if (part_num == 1) {
		 CFG_ENV_OFFSET = UBOOT_ENV1;
		 CFG_ENV_ADDR   = UBOOT_ENV1;
	 }
	 else if (part_num == 2) {
		 CFG_ENV_OFFSET = UBOOT_ENV2;
		 CFG_ENV_ADDR   = UBOOT_ENV2;
	 }
	 mvMPPConfigToSPI();
	 env_relocate_spec ();
	 mvMPPConfigToDefault();
 }

 /*
  * преобразование версии вида 1.0.10 в массив цифр, вида
  * sw_ver[0]=1, sw_ver[1]=0, sw_ver[2]=10,
  */
 void extract_fw_version	(char *fw_ver_str, char *fw_ver_arr,char *err_cnt )
 {
	 char tmp_str[20]={0};

	 (*err_cnt)++;

	 fw_ver_str += strlen(FW_PATTERN);
	 strncpy (tmp_str,fw_ver_str,strlen(fw_ver_str));

	 fw_ver_arr[0] = atoi (strtok (tmp_str, "."));
	 fw_ver_arr[1] = atoi (strtok (NULL, "."));
	 fw_ver_arr[2] = atoi (strtok (NULL, "."));
 }

void env_relocate (void)
{
	char new_fw_cnt_1[50]={0};
	char new_fw_cnt_2[50]={0};

	DECLARE_GLOBAL_DATA_PTR;

	DEBUGF ("%s[%d] offset = 0x%lx\n", __FUNCTION__,__LINE__,
		gd->reloc_off);

#ifdef CONFIG_AMIGAONEG3SE
	enable_nvram();
#endif

#ifdef ENV_IS_EMBEDDED
	/*
	 * The environment buffer is embedded with the text segment,
	 * just relocate the environment pointer
	 */
	env_ptr = (env_t *)((ulong)env_ptr + gd->reloc_off);
	DEBUGF ("%s[%d] embedded ENV at %p\n", __FUNCTION__,__LINE__,env_ptr);
#else
	/*
	 * We must allocate a buffer for the environment
	 */
	env_ptr = (env_t *)malloc (CFG_ENV_SIZE);
	DEBUGF ("%s[%d] malloced ENV at %p\n", __FUNCTION__,__LINE__,env_ptr);
#endif

	/*
	 * After relocation to RAM, we can always use the "memory" functions
	 */
	env_get_char = env_get_char_memory;

	if (gd->env_valid == 0) {
#if defined(CONFIG_GTH)	|| defined(CFG_ENV_IS_NOWHERE)	/* Environment not changable */
		puts ("Using default environment\n\n");
#else
		puts ("*** Warning - bad CRC, using default environment\n\n");
		SHOW_BOOT_PROGRESS (-1);
#endif

		if (CFG_ENV_OFFSET == UBOOT_ENV1){
			if (sizeof(default_environment_env1) > ENV_SIZE)
			{
				puts ("*** Error - default environment is too large\n\n");
				return;
			}

			memset (env_ptr, 0, sizeof(env_t));
			memcpy (env_ptr->data,
				default_environment_env1,
				sizeof(default_environment_env1));
		}
		else {
			if (sizeof(default_environment_env2) > ENV_SIZE)
			{
				puts ("*** Error - default environment is too large\n\n");
				return;
			}

			memset (env_ptr, 0, sizeof(env_t));
			memcpy (env_ptr->data,
				default_environment_env2,
				sizeof(default_environment_env2));
		}

#ifdef CFG_REDUNDAND_ENVIRONMENT
		env_ptr->flags = 0xFF;
#endif
		env_crc_update ();
		gd->env_valid = 1;
	}
	else {
#ifdef CONFIG_MARVELL
		unsigned int boot_env1,boot_env2;
		char *env;
		char *version_str1,*version_str2;
		char fw_ver_arr_1[3]={0},fw_ver_arr_2[3]={0},err_cnt=0;
		unsigned char prior_str[50];

		/* переключимся на работу с ENV1 */
		switch_to_another_partition(1);
        gd->env_addr = (ulong)&(env_ptr->data);

        /* флаг приоритета раздела 1 */
        boot_env1 = atoi (getenv("boot_priority"));

        /* флаг обновления ПО раздела 1 */
        strncpy (new_fw_cnt_1,getenv("new_fw_cnt"),15);

        /* переключимся на работу с ENV2 */
        switch_to_another_partition(2);
	    gd->env_addr = (ulong)&(env_ptr->data);

	    /* флаг приоритета раздела 2*/
        boot_env2 = atoi (getenv("boot_priority"));

        /* флаг обновления ПО раздела 2 */
        strncpy (new_fw_cnt_2,getenv("new_fw_cnt"),15);

        /* получим версию ПО из информации в uImage при битых Env1 и Env2 */
        ( strstr (kern_1_ver,FW_PATTERN) == NULL ) ? 0 : extract_fw_version (strstr (kern_1_ver,FW_PATTERN),fw_ver_arr_1,&err_cnt);
        ( strstr (kern_2_ver,FW_PATTERN) == NULL ) ? 0 : extract_fw_version (strstr (kern_2_ver,FW_PATTERN),fw_ver_arr_2,&err_cnt);

        /* если Env1 и Env2 оказались битыми то сравним версии ПО для принятия решиния о том
         * с какого раздела следует проводить загрузку */
        if (err_cnt == 2) {
        	printf ("Env1 and Env2 are damaged!\nCompare software version for select boot partition\n");
            printf ("kernel1_ver = %s, kernel2_ver = %s\n",kern_1_ver,kern_2_ver);

        	char i;

        	/* сравним версии ПО */
        	if (fw_ver_arr_1[0] > fw_ver_arr_2[0]) i = 1;
        	else
        		if (fw_ver_arr_1[0] < fw_ver_arr_2[0]) i = 2;
        		else
        			if (fw_ver_arr_1[1] > fw_ver_arr_2[1]) i = 1;
        			else
        				if (fw_ver_arr_1[1] < fw_ver_arr_2[1]) i = 2;
        				else
        					if (fw_ver_arr_1[2] > fw_ver_arr_2[2]) i = 1;
        					else
        						if (fw_ver_arr_1[2] < fw_ver_arr_2[2]) i = 2;
        						else // версии совпадают
        							i = 0;

        	if (i == 1) {
        		/* версия ПО раздела 1 больше версии ПО раздела 2. Изменим
        		 * приоритет загрузки разделов, если это требуется */
        		if (boot_env1 < boot_env2) {

        	        /* переключимся на работу с ENV2 */
        	        switch_to_another_partition(2);
        		    gd->env_addr = (ulong)&(env_ptr->data);

        		    boot_env2 = boot_env2 - 2;
        			itoa (boot_env1,prior_str);
        			setenv("boot_priority", prior_str);
        			saveenv();

        		}
        	}
        	else
        		if (i == 2) {
            		/* версия ПО раздела 2 больше версии ПО раздела 1. Изменим
            		 * приоритет загрузки разделов, если это требуется */
            		if (boot_env1 > boot_env2) {
            	        /* переключимся на работу с ENV1 */
            	        switch_to_another_partition(1);
            		    gd->env_addr = (ulong)&(env_ptr->data);

            		    boot_env1 = boot_env1 - 2;
            			itoa (boot_env1,prior_str);
            			setenv("boot_priority", prior_str);
            			saveenv();

            	        /* переключимся на работу с ENV2 */
            	        switch_to_another_partition(2);
            		}
        		}
        		else printf("sw_ver_arr_2=sw_ver_arr_1\n");
        }

        printf ("boot_priority_1 = %d, boot_priority_2 = %d\n",boot_env1,boot_env2);
        printf ("new_fw_cnt_1 = %s, new_fw_cnt_2 = %s\n",new_fw_cnt_1,new_fw_cnt_2);
//        printf ("%d.%d.%d\n",fw_ver_arr_1[0],fw_ver_arr_1[1],fw_ver_arr_1[2]);
//        printf ("%d.%d.%d\n",fw_ver_arr_2[0],fw_ver_arr_2[1],fw_ver_arr_2[2]);


        if (boot_env1 > boot_env2) {
        	if ((strncmp (new_fw_cnt_1, "start", 5) == 0)){

        		printf ("Upgrade error, back to old firmware!\n");

        		switch_to_another_partition(1);

    			setenv("new_fw_cnt", "upgrade_error");

    			itoa ((boot_env1-2),prior_str);
    			setenv("boot_priority", prior_str);
    			saveenv();

    			setenv("bootcmd", "");
        	}
        	else if (strncmp (new_fw_cnt_1, "new", 3) == 0){
               	printf ("boot_priority_1 > boot_priority_2 -> switch to ENV1\n");
               	switch_to_another_partition(1);
        	}
        	else if (strncmp (new_fw_cnt_1, "no_define", 9) == 0){
               	printf ("boot_priority_1 > boot_priority_2 -> switch to ENV1\n");
               	switch_to_another_partition(1);
        	}
        	else if (strncmp (new_fw_cnt_1, "upgrade_done", 12) == 0){
               	printf ("boot_priority_1 > boot_priority_2 -> switch to ENV1\n");
               	switch_to_another_partition(1);
        	}
    		else if (strncmp (new_fw_cnt_1, "upgrade_error", 13) == 0){
    			if (strncmp (new_fw_cnt_2, "upgrade_error", 13) == 0){
        			printf ("Both partition are damaged! Stop booting!\n");

        			switch_to_another_partition(1);

        			setenv("bootcmd", "");
    			}
    			else {
    				printf ("No describe for this case!\n");
    				setenv("bootcmd", "");
    			}

    		}
        }
        else {
        	if ((strncmp (new_fw_cnt_2, "start", 5) == 0)){
    			unsigned char prior_str[50];

    			printf ("Upgrade error, back to old firmware!\n");

    			setenv("new_fw_cnt", "upgrade_error");

    			itoa ((boot_env2-2),prior_str);
    			setenv("boot_priority", prior_str);
    			saveenv();

    			setenv("bootcmd", "");

        	}
        	else if (strncmp (new_fw_cnt_2, "new", 3) == 0){
        		printf ("boot_priority_1 < boot_priority_2 -> switch to ENV2\n");
        	}
        	else if (strncmp (new_fw_cnt_2, "no_define", 9) == 0){
        		printf ("boot_priority_1 < boot_priority_2 -> switch to ENV2\n");
        	}
        	else if (strncmp (new_fw_cnt_2, "upgrade_done", 12) == 0){
        		printf ("boot_priority_1 < boot_priority_2 -> switch to ENV2\n");
        	}
    		else if (strncmp (new_fw_cnt_2, "upgrade_error", 13) == 0){
    			if (strncmp (new_fw_cnt_1, "upgrade_error", 13) == 0){
        			printf ("Both partition are damaged! Stop booting!\n");
        			setenv("bootcmd", "");
    			}
    			else {
    				printf ("No describe for this case!\n");
    				setenv("bootcmd", "");
    			}
    		}
        }

    	if ((strncmp (new_fw_cnt_1, "new", 3) == 0) || (strncmp (new_fw_cnt_2, "new", 3) == 0)) {
    		printf ("First boot after upgrade!\n");
    		setenv("new_fw_cnt", "start");
    		saveenv();
    	}
#else
        env_relocate_spec ();
#endif
	}
	gd->env_addr = (ulong)&(env_ptr->data);

#ifdef CONFIG_AMIGAONEG3SE
	disable_nvram();
#endif
}

#ifdef CONFIG_AUTO_COMPLETE
int env_complete(char *var, int maxv, char *cmdv[], int bufsz, char *buf)
{
	int i, nxt, len, vallen, found;
	const char *lval, *rval;

	found = 0;
	cmdv[0] = NULL;

	len = strlen(var);
	/* now iterate over the variables and select those that match */
	for (i=0; env_get_char(i) != '\0'; i=nxt+1) {

		for (nxt=i; env_get_char(nxt) != '\0'; ++nxt)
			;

		lval = (char *)env_get_addr(i);
		rval = strchr(lval, '=');
		if (rval != NULL) {
			vallen = rval - lval;
			rval++;
		} else
			vallen = strlen(lval);

		if (len > 0 && (vallen < len || memcmp(lval, var, len) != 0))
			continue;

		if (found >= maxv - 2 || bufsz < vallen + 1) {
			cmdv[found++] = "...";
			break;
		}
		cmdv[found++] = buf;
		memcpy(buf, lval, vallen); buf += vallen; bufsz -= vallen;
		*buf++ = '\0'; bufsz--;
	}

	cmdv[found] = NULL;
	return found;
}
#endif
